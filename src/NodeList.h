//noah
#ifndef __NodeList__
#define __NodeList__

#include <iostream>

//#include "Iterator.h" 
using namespace std;

typedef int Elem; 

class NodeList { // node-based list 
  private: 
struct Node{
  Elem elem;
  Node* prev;
  Node* next;
};
  public: 
class Iterator { // an iterator for the list 
  public: 
    Elem& operator*(); // reference to the element 
    bool operator==(const Iterator& p) const; // compare positions 
    bool operator!=(const Iterator& p) const; Iterator& operator++(); // move to next position 
    Iterator& operator--(); // move to previous position 
    friend class NodeList; // give NodeList access 
  private: Node* v; // pointer to the node 
    Iterator(Node* u); // create from node 
};
  public: 
// default constructor 
    NodeList(); int size() const; // list size 
    bool empty() const; // is the list empty? 
    Iterator begin() const; // beginning position 
    Iterator end() const; // (just beyond) last position 
    void insertFront(const Elem& e); // insert at front 
    void insertBack(const Elem& e); // insert at rear 
    void insert(const Iterator& p, const Elem& e); // insert e before p 
    void eraseFront(); // remove first 
    void eraseBack(); // remove last 
    void erase(const Iterator& p); // remove p 
  private: 
// data members 
    int n; // number of items 
    Node* header; // head-of-list sentinel 
    Node* trailer; // tail-of-list sentinel 
};

#endif