//noah mcmillan
#include "BinaryTree.h"

BinaryTree::Node::Node(const Type& item, Node *l, Node *r, Node *p){
  data = item;
  left = l;
  right = r;
  parent = p;
}


BinaryTree::Node::Node(Node *l, Node *r, Node *p){
  left = l;
  right = r;
  parent = p;
}

BinaryTree::BinaryTree(){
  root = nullptr;
}


BinaryTree::~BinaryTree(){
  if (root != nullptr)
    deleteNodes(root);
}

//deallocates mem for nodes in post order to visit all children before root
void BinaryTree::deleteNodes(Node *ptr){
  if (ptr == nullptr)
    return;

  deleteNodes(ptr->left);
  deleteNodes(ptr->right);
  delete ptr;
}

template<class Type>
//goes down tree then inserts num passed in
void BinaryTree::insert(const Type& item) {
  if (root == nullptr) {
    root = new Node(item);
    return;
  }

  Node *temp = root;

  while (true) {
    if (item < temp->data) {
      if (temp->left == nullptr) {
        temp->left = new Node(item);
        temp->left->parent = temp;
          return;
      }
      temp = temp->left;
    } else if (item > temp->data) {
      if (temp->right == nullptr) {
          temp->right = new Node(item);
          temp->right->parent = temp;
          return;
      }
      temp = temp->right;
    }else{
      return;
    }
  }
}


int BinaryTree::inOrder(BinaryTree::Node *ptr) const{
  if (ptr == nullptr)
    return 0;

  inOrder(ptr->left);
  cout << ptr->data << " ";
  inOrder(ptr->right);
  return 0;
}


void BinaryTree::print(void) const{
  cout << "root: " << root->data << endl;
  if (root != nullptr)
    inOrder(root);

  cout << endl;
}

//overloaded version to print out specificaly the ptr returned from searchTree
void BinaryTree::print(Node *ptr) const{
  if (ptr == nullptr){
    cout << "Number not found" << endl;
    return;
  }

  cout << ptr->data << endl;
}

//overload of sizeOfTree to obfuscate it from non member funcs
int BinaryTree::sizeOfTree(BinaryTree::Node *ptr, int &i) const{
  if (ptr == nullptr)
    return i;

  sizeOfTree(ptr->left, i);
  sizeOfTree(ptr->right, i);
  i++;
  return i;
}

//function avalible as is not private, passes in node ptr into the overloaded version
int BinaryTree::sizeOfTree(void){
  int i = 0;
  return sizeOfTree(root, i);
}

template<class Type>
//searches for node matching data that is passed into, if it is not = then recursively searches
BinaryTree::Node *BinaryTree::searchTree(const Type& itemToSearch){
  return searchTree(itemToSearch, root);
}

template<class Type>
//private overloaded version as to allow to pass in ptr without exposing it to non member executions and to allow for it to run recursively
BinaryTree::Node *BinaryTree::searchTree(const Type& itemToSearch, BinaryTree::Node *ptr){
  if (ptr == nullptr || ptr->data == itemToSearch)
    return ptr;

  if (ptr->data > itemToSearch){
    return searchTree(itemToSearch, ptr->left);
  } else if (ptr->data < itemToSearch){
    return searchTree(itemToSearch, ptr->right);
  } else {
    return nullptr;
  }
}

template<class Type>
//determent if leaf node or internal node then calls appropriate func
void BinaryTree::removeNode(const Type& itemToRemove){
  Node *itemsNode = searchTree(itemToRemove);

  if (itemsNode == nullptr)
    return;

  if (itemsNode->left == nullptr && itemsNode->right == nullptr){
    removeLeaf(itemsNode);
  } else {
    removeInternal(itemsNode);
  }
}

//removes the node if it is a leaf
void BinaryTree::removeLeaf(BinaryTree::Node *ptr){
  Node *ptrParent = ptr->parent;
  if (ptrParent->left == ptr){
    delete ptrParent->left;
    ptrParent->left = nullptr;
  }else if (ptrParent->right == ptr){
    delete ptrParent->right;
    ptrParent->right = nullptr;
  }
}

//removes node if it is internal
void BinaryTree::removeInternal(BinaryTree::Node *ptr){
  Node *ptrParent = ptr->parent;
  //removes internal node that has external child node, same as textbooks figure 10.5
  if (ptr->left == nullptr){
    if (ptrParent->left == ptr){
      ptrParent->left = ptr->right;
      delete ptr;
    }else if (ptrParent->right == ptr){
      ptrParent->right = ptr->right;
      delete ptr;
    }
  } else if (ptr->right == nullptr){
    if (ptrParent->left == ptr){
      ptrParent->left = ptr->left;
     delete ptr;
    }else if (ptrParent->right == ptr){
      ptrParent->right = ptr->left;
      delete ptr;
    }
  //removes internal node that has no external children, both children are internal nodes same as textbooks figure 10.6
  } else {

    Node *leftMostPtrNode = leftMostNode(ptr->right);


    leftMostPtrNode->parent->left = leftMostPtrNode->right;

    if (ptr != root)
      leftMostPtrNode->parent = ptrParent;

    leftMostPtrNode->left = ptr->left;
    leftMostPtrNode->right =  ptr->right;

    if (ptr == root){
      root = leftMostPtrNode;
    }else if (ptrParent->left == ptr){
      ptrParent->left = leftMostPtrNode;
    }else if (ptrParent->right == ptr){
      ptrParent->right = leftMostPtrNode;
    }


    delete ptr;

  }
}

//returns leftmost node of the ptr that is passed in as that way it is less than the right ptr it is replacing and more than the left ptr
BinaryTree::Node *BinaryTree::leftMostNode(BinaryTree::Node *ptr){
  if (ptr->left == nullptr)
    return ptr;BinaryTree::Node::Node(const Type& item, Node *l, Node *r, Node *p){
    data = item;
    left = l;
    right = r;
    parent = p;
  }


  BinaryTree::Node::Node(Node *l, Node *r, Node *p){
    left = l;
    right = r;
    parent = p;
  }

  BinaryTree::BinaryTree(){
    root = nullptr;
  }


  BinaryTree::~BinaryTree(){
    if (root != nullptr)
      deleteNodes(root);
  }

  //deallocates mem for nodes in post order to visit all children before root
  void BinaryTree::deleteNodes(Node *ptr){
    if (ptr == nullptr)
      return;

    deleteNodes(ptr->left);
    deleteNodes(ptr->right);
    delete ptr;
  }


  return leftMostNode(ptr->left);
}
