//noah
#include "NodeList.h"

NodeList::NodeList() { 
  n = 0; 
  header = new Node; 
  trailer = new Node; 
  header->next = trailer; 
  trailer->prev = header; 
}// constructor // initially empty // create sentinels 

int NodeList::size() const { return n; }// list size 

bool NodeList::empty() const { return (n == 0); }// is the list empty? 

NodeList::Iterator NodeList::begin() const { return Iterator(header->next); }// begin position is first item 

NodeList::Iterator NodeList::end() const { return Iterator(trailer); }

void NodeList::insert(const NodeList::Iterator& p, const Elem& e) { 
  Node* w = p.v; // p’s node 
  Node* u = w->prev; // p’s predecessor // new node to insert 
  Node* v = new Node; 
  v->elem = e; 
  v->next = w; 
  w->prev = v; // link in v before w 
  v->prev = u; u->next = v; // link in v after u 
  n++; 
} 

void NodeList::insertFront(const Elem& e){ // insert at front 
  insert(begin(), e); 
} 

void NodeList::insertBack(const Elem& e) { 
  insert(end(), e); 
}