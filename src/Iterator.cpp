//noah
#include "NodeList.h"

NodeList::Iterator::Iterator(Node* u) { v = u; }// constructor from Node* 

Elem& NodeList::Iterator::operator*() { return v->elem; }// reference to the element 
// compare positions 
bool NodeList::Iterator::operator==(const Iterator& p) const { return v == p.v; } 

bool NodeList::Iterator::operator!=(const Iterator& p) const { return v != p.v; } 
// move to next position 
NodeList::Iterator& NodeList::Iterator::operator++() { v = v->next; return *this; } // move to previous position 

NodeList::Iterator& NodeList::Iterator::operator--() { v = v->prev; return *this; }