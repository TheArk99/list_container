//noah
#include <iostream>
#include <cstdlib>
#include <string>
//#include "Iterator.h"
//#include "NodeList.h"
#include "BinaryTree.h"
using namespace std;

typedef int DataType;

int main(int argc, char **argv){
  BinaryTree<DataType> bt;


  //tree from 10.6
  bt.insert(44);
  //BinaryTree<DataType>::Iterator p = bt.begin();
  //BinaryTree<DataType>::Iterator q;
  //cout << *p << endl;
  bt.insert(17);
  bt.insert(88);
  bt.insert(65);
  bt.insert(97);
  bt.insert(32);
  bt.insert(28);
  bt.insert(29);
  bt.insert(82);
  bt.insert(54);
  bt.insert(76);
  bt.insert(75);
  bt.insert(72);
  bt.insert(80);
  bt.insert(78);

  bool exitLoop = false;

  while (!exitLoop){
    cout << "Which option to do:" << endl;
    cout << "1.) insert number into tree" << endl;
    cout << "2.) search tree" << endl;
    cout << "3.) delete number from tree" << endl;
    cout << "4.) print tree" << endl;
    cout << "5.) minimum in tree" << endl;
    cout << "6.) maximum in tree" << endl;
    cout << "7.) exit" << endl;
    cout << ">>> ";
    int userInput;
    cin >> userInput;
    if (!(userInput >= 1 && userInput <= 7)){
      cout << endl << "Enter correct option!!!" << endl;
      system("sleep 1");
      system("clear");
      continue;
    }

    switch(userInput){
      case 1:
        cout << "Enter number you want to insert: ";
        cin >> userInput;
        bt.insert(userInput);
        break;
      case 2:
        cout << "Enter number: ";
        cin >> userInput;
        bt.print(bt.searchTree(userInput));
        break;
      case 3:
        cout << "Enter number to delete: ";
        cin >> userInput;
        bt.removeNode(userInput);
        break;
      case 4:
        bt.print();
        break;
      case 5:
        cout << "The mimimum num in tree is: ";
        bt.print(bt.minimum());
        break;
      case 6:
        cout << "The maximum num in tree is: ";
        bt.print(bt.end());
        break;
      case 7:
        exitLoop = true;
        break;
    }

    cout << endl << "Press Enter to Continue";
    //geting first enter
    cin.get();
    //getting second and acctual enter
    cin.get();
    system("clear");
  }


  return 0;
}
