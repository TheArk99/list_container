//noah mcmillan

#ifndef __BINARYTREE__
#define __BINARYTREE__

#include <iostream>
using namespace std;

//typedef int Type;
template<class Type>

class BinaryTree{
//class Node and Iterator
////////////////////////////////////////////////////////////////
  private:
    class Node {
      public:
        Type data;
        Node *left;
        Node *right;
        Node *parent;

        Node(const Type& item, Node *l = nullptr, Node *r = nullptr, Node *p = nullptr){
          data = item;
          left = l;
          right = r;
          parent = p;
        }
        Node(Node *l = nullptr, Node* r = nullptr, Node* p = nullptr){
          left = l;
          right = r;
          parent = p;
        }
    };

    Node *root;

  public:

    class Iterator { // an iterator for the list
      public:
        Iterator(); 	   							// constructor with NULL
        Iterator(Node *u); 							// constructor from node
        Type& operator*() const; // reference to the element
        bool operator==(const Iterator& p) const; // compare positions
        bool operator!=(const Iterator& p) const;
        Iterator& operator++(); // move to next position
        Iterator& operator--(); // move to previous position
        friend class BinaryTree;
      private:
        Node* v; // pointer to the node
    };
////////////////////////////////////////////////////////////////

  private:

    //private overloaded version as to allow to pass in ptr without exposing it to non member executions and to allow for it to run recursively
    Node *searchTree(const Type& itemToSearch, Node *ptr){
      if (ptr == nullptr || ptr->data == itemToSearch)
        return ptr;

      if (ptr->data > itemToSearch){
        return searchTree(itemToSearch, ptr->left);
      } else if (ptr->data < itemToSearch){
        return searchTree(itemToSearch, ptr->right);
      } else {
        return nullptr;
      }
    }

    //deallocates mem for nodes in post order to visit all children before root
    void deleteNodes(Node *ptr){
      if (ptr == nullptr)
        return;

      deleteNodes(ptr->left);
      deleteNodes(ptr->right);
      delete ptr;
    }

    //removes the node if it is a leaf
    void removeLeaf(Node *ptr){
      Node *ptrParent = ptr->parent;
      if (ptrParent->left == ptr){
        delete ptrParent->left;
        ptrParent->left = nullptr;
      }else if (ptrParent->right == ptr){
        delete ptrParent->right;
        ptrParent->right = nullptr;
      }
    }

    //removes node if it is internal
    void removeInternal(Node *ptr){
      Node *ptrParent = ptr->parent;
      //removes internal node that has external child node, same as textbooks figure 10.5
      if (ptr->left == nullptr){
        if (ptrParent->left == ptr){
          ptrParent->left = ptr->right;
          delete ptr;
        }else if (ptrParent->right == ptr){
          ptrParent->right = ptr->right;
          delete ptr;
        }
      } else if (ptr->right == nullptr){
        if (ptrParent->left == ptr){
          ptrParent->left = ptr->left;
         delete ptr;
        }else if (ptrParent->right == ptr){
          ptrParent->right = ptr->left;
          delete ptr;
        }
      //removes internal node that has no external children, both children are internal nodes same as textbooks figure 10.6
      } else {

        Node *leftMostPtrNode = leftMostNode(ptr->right);


        leftMostPtrNode->parent->left = leftMostPtrNode->right;

        if (ptr != root)
          leftMostPtrNode->parent = ptrParent;

        leftMostPtrNode->left = ptr->left;
        leftMostPtrNode->right =  ptr->right;

        if (ptr == root){
          root = leftMostPtrNode;
        }else if (ptrParent->left == ptr){
          ptrParent->left = leftMostPtrNode;
        }else if (ptrParent->right == ptr){
          ptrParent->right = leftMostPtrNode;
        }


        delete ptr;

      }
    }

    //returns leftmost node of the ptr that is passed in as that way it is less than the right ptr it is replacing and more than the left ptr
    Node *leftMostNode(Node *ptr){
      if (ptr->left == nullptr)
        return ptr;

      return leftMostNode(ptr->left);
    }

    //overload of sizeOfTree to obfuscate it from non member funcs
    int sizeOfTree(Node *ptr, int &i) const{
      if (ptr == nullptr)
        return i;

      sizeOfTree(ptr->left, i);
      sizeOfTree(ptr->right, i);
      i++;
      return i;
    }

//end of private members
////////////////////////////////////////////////////////////////
  public:

//constructor && destructor
    BinaryTree(){
      root = nullptr;
    }

    ~BinaryTree(){
      if (root != nullptr)
        deleteNodes(root);
    }

////////////////////////////////////////////////////////////////

    //goes down tree then inserts num passed in
    void insert(const Type& item) {
      if (root == nullptr) {
        root = new Node(item);
        return;
      }

      Node *temp = root;

      while (true) {
        if (item < temp->data) {
          if (temp->left == nullptr) {
            temp->left = new Node(item);
            temp->left->parent = temp;
              return;
          }
          temp = temp->left;
        } else if (item > temp->data) {
          if (temp->right == nullptr) {
              temp->right = new Node(item);
              temp->right->parent = temp;
              return;
          }
          temp = temp->right;
        }else{
          return;
        }
      }
    }

    //regular print, prints full tree in order
    void print(void) const{
      cout << "root: " << root->data << endl;
      if (root != nullptr)
        inOrder(root);

      cout << endl;
    }

    //overloaded version to print out specificaly the ptr returned from searchTree
    //void print(Node *ptr) const{
    void print(const Iterator& ptr) const{
      if (ptr == nullptr){
        cout << "Number not found" << endl;
        return;
      }

      cout << *ptr << endl;
    }

    int inOrder(Node *ptr) const{
      if (ptr == nullptr)
        return 0;

      inOrder(ptr->left);
      cout << ptr->data << " ";
      inOrder(ptr->right);
      return 0;
    }

    //searches for node matching data that is passed into, if it is not = then recursively searches
//    Node *searchTree(const Type& itemToSearch){
    Iterator searchTree(const Type& itemToSearch){
      return Iterator(searchTree(itemToSearch, root));
    }

    //determent if leaf node or internal node then calls appropriate func
    void removeNode(const Type& itemToRemove){
      Node *itemsNode = searchTree(itemToRemove, root);

      if (itemsNode == nullptr)
        return;

      if (itemsNode->left == nullptr && itemsNode->right == nullptr){
        removeLeaf(itemsNode);
      } else {
        removeInternal(itemsNode);
      }
    }

    //function avalible as is not private, passes in node ptr into the overloaded version
    int sizeOfTree(void){
      int i = 0;
      return sizeOfTree(root, i);
    }

    //returns iterator to minimum node
    Iterator minimum(void){
      return Iterator(leftMostNode(root));
    }


///////////////////////////////////////////////////////////////////
//iterator functions that are public BinaryTree members
    //returns root
    Iterator begin() const { // begin position is first item
      return Iterator(root);
    }

    //returns maximum num in tree
    Iterator end() const { // end position is just beyond last
      Node *temp = root;
      while (temp->right != nullptr){
        temp = temp->right;
      }
      return Iterator(temp);
    }
};

///////////////////////////////////////////////////////////////////
// Iterator class template definition.


// constructor with NULL
template <class Type>
BinaryTree<Type>::Iterator::Iterator() {
  v = nullptr;
}

// constructor from Node*
template <class Type>
BinaryTree<Type>::Iterator::Iterator(Node* u) {
  v = u;
}

// reference to the element
template <class Type>
Type& BinaryTree<Type>::Iterator::operator*() const{
  return v->data;
}

// compare positions
template <class Type>
bool BinaryTree<Type>::Iterator::operator==(const Iterator& p) const {
  return v == p.v;
}

template <class Type>
bool BinaryTree<Type>::Iterator::operator!=(const Iterator& p) const {
  return v != p.v;
}

// move to next position
template <class Type>
typename BinaryTree<Type>::Iterator& BinaryTree<Type>::Iterator::operator++() {
  v = v->right; return *this;
}

// move to previous position
template <class Type>
typename BinaryTree<Type>::Iterator& BinaryTree<Type>::Iterator::operator--() {
  v = v->parent; return *this;
}


#endif
