//noah
#ifndef __Iterator__
#define __Iterator__

#include <iostream>
#include "NodeList.h"  
using namespace std;

typedef int Elem;

struct Node{
  Elem elem;
  Node* prev;
  Node* next;
};

class Iterator { // an iterator for the list 
  public: 
    Elem& operator*(); // reference to the element 
    bool operator==(const Iterator& p) const; // compare positions 
    bool operator!=(const Iterator& p) const; Iterator& operator++(); // move to next position 
    Iterator& operator--(); // move to previous position 
    friend class NodeList; // give NodeList access 
  private: Node* v; // pointer to the node 
    Iterator(Node* u); // create from node 
};

#endif