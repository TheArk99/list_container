CXX := g++
CXXFlags := -Wall -g -pipe -O2 -march=native -D_GLIBCXX_ASSERTIONS -std=c++20

.PHONY: all clean obj main

all: main

obj:
	$(CXX) -c src/NodeList.cpp -o objfiles/NodeList.o $(CXXFlags)
	$(CXX) -c src/Iterator.cpp -o objfiles/Iterator.o $(CXXFlags)
	$(CXX) -c src/BinaryTree.cpp -o objfiles/BinaryTree.o $(CXXFlags)

main: 
	$(CXX) -o bin/main src/main.cpp src/BinaryTree.h $(CXXFlags)

install: clean
	mkdir -p bin/
	mkdir -p objfiles/
	$(MAKE) all

clean:
	rm -rf bin/
